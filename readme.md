# How to run

You need docker and docker-compose on your local machine
To start project you must run commands:
```bash
docker-compose build
docker-compose run app sh -c "python manage.py migrate"
docker-compose run app sh -c "python manage.py loaddata books.json"
docker-compose up
```
Application start at 8000 port

## Create superuser
```bash
docker-compose run app sh -c "python manage.py createsuperuser"
```
## Run tests
```bash
docker-compose run app sh -c "python manage.py test"
```
## Run command 'order_books'
```bash
docker-compose run app sh -c "python manage.py order_books asc"
```
or
```bash
docker-compose run app sh -c "python manage.py order_books desc"
```