from django.shortcuts import render
from django.core.paginator import Paginator
from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.forms.models import model_to_dict

from .models import Book
from .forms import BookForm
from core.models import WebRequest


BOOKS_PER_PAGE = 10
WEB_REQUEST_LIMIT = 10
INDEX_TITLE = 'Books list'
CREATE_BOOK_TITLE = 'Create Book'
EDIT_BOOK_TITLE = 'Edit Book'


def index(request):
    page_number = request.GET.get('page', 1)

    books = Book.objects.all()

    paginator = Paginator(books, BOOKS_PER_PAGE)

    books = paginator.get_page(page_number)

    web_requests = WebRequest.objects.all()\
        .order_by('-time')[:WEB_REQUEST_LIMIT]

    return render(request, 'index.html', {
        'title': INDEX_TITLE,
        'books': books,
        'web_requests': web_requests,
    })


def create_book(request):

    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "New Book was created")
    else:
        form = BookForm()

    return render(request, 'bookstore/create_book.html', {
        'title': CREATE_BOOK_TITLE,
        'form': form,
    })


def edit_book(request, pk):
    book = get_object_or_404(Book, pk=pk)

    if request.method == 'POST':
        form = BookForm(request.POST, instance=book)
        if form.is_valid():
            form.save()
            messages.success(request, 'Book was edited')
    else:
        form = BookForm(instance=book)

    return render(request, 'bookstore/edit_book.html', {
        'title': EDIT_BOOK_TITLE,
        'form': form,
    })
