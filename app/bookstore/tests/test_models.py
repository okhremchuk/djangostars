from django.test import TestCase
from django.core.exceptions import ValidationError
from datetime import date

from bookstore.models import Book


class ModelTests(TestCase):

    def setUp(self):
        self.publish_date = date.today()

    def test_book_str(self):
        book = Book.objects.create(
            title="Some title",
            authors_info="Some author",
            isbn="8765432123456",
            price=11.10,
            publish_date=self.publish_date
        )

        self.assertEqual(str(book), book.title)

    def test_isbn_validation(self):
        Book.objects.create(
            title="Some Title",
            authors_info="Some Author",
            isbn="85674",
            price=11.10
        )

        self.assertRaises(ValidationError)

    def test_price_validation(self):
        Book.objects.create(
            title="Some Title",
            authors_info="some author",
            isbn="8765432123456",
            price=11.678
        )

        self.assertRaises(ValidationError)
