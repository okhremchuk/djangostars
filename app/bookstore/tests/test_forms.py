from django.test import TestCase
from datetime import date

from bookstore.forms import BookForm


class FormTests(TestCase):

    def setUp(self):
        self.publish_date = date.today()

    def test_valid_data(self):
        form = BookForm({
            'title': 'Some title',
            'authors_info': 'Some author',
            'isbn': '8765432123456',
            'price': 11.98,
            'publish_date': self.publish_date
        })

        self.assertTrue(form.is_valid())
        book = form.save()
        self.assertEqual(book.title, 'Some title')
        self.assertEqual(book.authors_info, 'Some author')
        self.assertEqual(book.isbn, '8765432123456')
        self.assertEqual(float(book.price), 11.98)
        self.assertEqual(book.publish_date, self.publish_date)

    def test_blank_data(self):
        form = BookForm({})

        self.assertFalse(form.is_valid())
