from django.urls import path

from . import views

urlpatterns = [
    path('', views.create_book, name="create_book"),
    path('<int:pk>/', views.edit_book, name="edit_book"),
]