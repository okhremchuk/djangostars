from django.contrib import admin

from .models import Book


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    ordering = ('-publish_date',)
    list_display = ('title', 'authors_info', 'price')
