from django.db import models
from django.utils.translation import gettext as _
from django.utils.timezone import now
import logging

from .validators import validate_isbn


ISBN_LENGTH = 13
MAX_PRICE_DIGITS = 6
MAX_PRICE_DECIMAL_PLACES = 2

logger = logging.getLogger(__name__)


class Book(models.Model):
    title = models.TextField(verbose_name=_('title'))
    authors_info = models.TextField(verbose_name=_('authors information'))
    isbn = models.CharField(verbose_name=_('ISBN'), max_length=ISBN_LENGTH,
                            validators=[validate_isbn])
    price = models.DecimalField(verbose_name=_('price'),
                                max_digits=MAX_PRICE_DIGITS,
                                decimal_places=MAX_PRICE_DECIMAL_PLACES)
    publish_date = models.DateField(verbose_name=_('publish date'),
                                    default=now)

    def __str__(self):
        return self.title

    def __repr__(self):
        return f"<Book {self.pk}"

    def delete(self, using=None, keep_parents=False):
        logger.info(f"Deleted book: {self.title}")
        super(Book, self).delete(using=using, keep_parents=keep_parents)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.pk is None:
            logger.info(f"Created book: {self.title}")
        else:
            logger.info(f"Updated book: {self.title}")

        super(Book, self).save(force_insert=force_insert,
                               force_update=force_update, using=using,
                               update_fields=update_fields)
