from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_isbn(value):
    if len(value) != 13:
        raise ValidationError(_("Invalid ISBN: Wrong length"))

    try:
        int(value)
    except ValueError:
        raise ValidationError(_("Invalid ISBN: Value is not integer"))

    return True
