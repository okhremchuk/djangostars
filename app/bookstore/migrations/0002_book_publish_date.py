# Generated by Django 2.1.7 on 2019-03-02 09:47

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('bookstore', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='publish_date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='publish date'),
        ),
    ]
