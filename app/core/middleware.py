from .models import WebRequest


class WebRequestMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        self.save(request, response)
        return response

    def __repr__(self):
        return '<WebRequestMiddleware>'

    def process_response(self, request, response):

        if request.path.endswith('/favicon.ico'):
            return response

    def save(self, request, response):

        meta = request.META.copy()
        meta.pop('QUERY_STRING', None)
        meta.pop('HTTP_COOKIE', None)

        WebRequest(
            host=request.get_host(),
            path=request.path,
            method=request.method,
            uri=request.build_absolute_uri(),
            status_code=response.status_code,
            user_agent=meta.pop('HTTP_USER_AGENT', None),
            remote_addr=meta.pop('REMOTE_ADDR', None)
        ).save()
