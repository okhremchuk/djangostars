from django.core.management.base import BaseCommand
from bookstore.models import Book


ORDER_TYPES = ('asc', 'desc')
ORDER_LENGTH_LIMIT = 10


class Command(BaseCommand):

    def __repr__(self):
        return '<CommandOrderBook>'

    def add_arguments(self, parser):
        parser.add_argument('order_type', type=str, help="Sort type ('asc' or 'desc')")

    def handle(self, *args, **kwargs):
        order_type = kwargs['order_type']

        if order_type in ORDER_TYPES:

            order_by = 'publish_date'

            if order_type == 'desc':
                order_by = f"-{order_by}"

            books = Book.objects.all().order_by(order_by)[:ORDER_LENGTH_LIMIT]

            for book in books:
                self.stdout.write(str(book.publish_date))

        else:
            self.stderr.write("Only 'asc' or 'desc'")
