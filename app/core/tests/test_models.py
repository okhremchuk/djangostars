from django.test import TestCase

from core.models import WebRequest


PATH = '/'
STATUS_CODE = 200


def create_web_request():
    web_request = WebRequest.objects.create(
        host='127.0.0.1',
        path=PATH,
        method='GET',
        uri='127.0.0.1/',
        status_code=STATUS_CODE,
        user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
                   'AppleWebKit/537.36 (KHTML, like Gecko) ',
        remote_addr='255.255.255.255'
    )
    return web_request


class ModelTests(TestCase):

    def test_web_request_str(self):
        web_request = create_web_request()

        self.assertEqual(str(web_request), f"{PATH} - {STATUS_CODE}")
